<?php

namespace GrotTheCheat\Tests;

use GrotTheCheat\YamlConfig\YamlConfig;
use GrotTheCheat\YamlConfig\Exceptions\FileNotFoundException;
use PHPUnit\Framework;
use Symfony\Component\Yaml\Yaml as YamlParser;
use Symfony\Component\Yaml\Exception\ParseException;

class YamlConfigTest extends Framework\TestCase
{
    public function testConstructor()
    {
        try {
            $config = new YamlConfig('test.yaml');
            $this->assertTrue(true);
        } catch (\Exception $ex) {
            $this->fail("Exception thrown: " . $ex->getMessage());
        }
    }

    public function testConstructorContents()
    {
        $config = new YamlConfig('test.yaml');
        $keys = $config->keys();
        $key_count = count($keys);
        $this->assertSame(4, $key_count);
    }

    public function testConstructorException()
    {
        $this->expectException(FileNotFoundException::class);
        $config = new YamlConfig('does_not_exist.yaml');
    }

    public function testDump()
    {
        $config = new YamlConfig('test.yaml');
        $config->set('vendor', 'vendor-name-changed');
        $config->dump();

        $config = new YamlConfig('test.yaml');
        $value = $config->get('vendor');
        $config->set('vendor', 'vendor-name');
        $config->dump();

        $this->assertSame('vendor-name-changed', $value);
    }

    public function testEnsureTestFile()
    {
        try {
            $results = YamlParser::parseFile('test.yaml');
            $this->assertTrue(true);
        } catch (ParseException $ex) {
            fwrite(STDOUT, $ex->getMessage());
            $this->fail("The test file has errors");
        }
    }

    public function testGetFirstValue()
    {
        $config = new YamlConfig('test.yaml');
        $value = $config->get('vendor');
        $this->assertSame('vendor-name', $value);
    }

    public function testGetSecondValue()
    {
        $config = new YamlConfig('test.yaml');
        $value = $config->get('package');
        $this->assertSame('package-name', $value);
    }

    public function testGetDot()
    {
        $config = new YamlConfig('test.yaml');
        $value = $config->get('setting1.setting2.setting3');
        $this->assertSame('has-value', $value);
    }

    public function testSet()
    {
        $config = new YamlConfig('test.yaml');
        $config->set('vendor-name', 'test-set');
        $value = $config->get('vendor-name');
        $this->assertSame('test-set', $value);
    }

    public function testSetDot()
    {
        $config = new YamlConfig('test.yaml');
        $config->set('setting1.setting2.setting3', 'test-set');
        $value = $config->get('setting1.setting2.setting3');
        $this->assertSame('test-set', $value);
    }

    public function testLoad()
    {
        $config = YamlConfig::load('test.yaml');
        $this->assertInstanceOf(YamlConfig::class, $config);
    }

    public function testLoadFail()
    {
        $config = YamlConfig::load('file_does_not_exist.yaml');
        $this->assertFalse($config);
    }
}
