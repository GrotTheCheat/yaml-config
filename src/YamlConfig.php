<?php

namespace GrotTheCheat\YamlConfig;

use GrotTheCheat\YamlConfig\Exceptions\FileNotFoundException;
use Symfony\Component\Yaml\Yaml as YamlParser;

/** @noinspection PhpUnused */
class YamlConfig
{
    private $data = [];
    private $file_path = null;

    public function __construct(string $file_path = null)
    {
        if ($file_path === null)
            return;

        if (!file_exists($file_path)) {
            throw new FileNotFoundException("YamlConfig could not find the config file '" . $file_path . "'.");
        }

        $this->parseFile($file_path);
    }

    /** @noinspection PhpUnused */
    public function dump()
    {
        $yaml = YamlParser::dump($this->data, 5);
        file_put_contents($this->file_path, $yaml);
    }

    /** @noinspection PhpUnused */
    public function get($key)
    {
        $key_path_parts = explode('.', $key);

        return $this->getRecursive($key_path_parts, $this->data);
    }

    private function getRecursive($keys, $current_position)
    {
        $key = array_shift($keys);

        if (count($keys) == 0) {
            return $current_position[$key];
        } else {
            return $this->getRecursive($keys, $current_position[$key]);
        }
    }

    /** @noinspection PhpUnused */
    public function keys() : array
    {
        return self::keysRecursive($this->data);
    }

    private function keysRecursive($array, $prefix = '') : array
    {
        $result = [];
        foreach($array as $key=>$value) {
            if(is_array($value)) {
                $result = $result + self::keysRecursive($value, $prefix . $key . '.');
            }
            else {
                $result[$prefix . $key] = $value;
            }
        }
        return $result;
    }

    public function keyExists(string $key) : bool
    {
        return $this->keysExists([$key]);
    }

    public function keysExists(array $key) : bool
    {
        $keys = $this->keys();

        foreach ($keys as $key) {
            if (!in_array($key, $keys)) {
                return false;
            }
        }

        return true;
    }

    /** @noinspection PhpUnused */
    public static function load(string $file_path, bool $create_if_not_found = false)
    {
        $config = new self();

        if (file_exists($file_path)) {
            $config->parseFile($file_path);
        } else {
            if (!$create_if_not_found)
                return false;
        }

        return $config;
    }

    private function parseFile($file_path)
    {
        $data = YamlParser::parseFile($file_path);
        $this->setData($data);
        $this->setFilePath($file_path);
    }

    /** @noinspection PhpUnused */
    public function set($key, $value)
    {
        $key_parts = explode('.', $key);

        $this->setRecursive($key_parts, $value, $this->data);
    }

    private function setRecursive($keys, $value, &$current_position)
    {
        $key = array_shift($keys);

        if (count($keys) === 0) {
            $current_position[$key] = $value;
        } else {
            if (!isset($current_position[$key])) {
                $current_position[$key] = [];
            }

            $this->setRecursive($keys, $value, $current_position[$key]);
        }
    }

    private function setData($data)
    {
        $this->data = $data;
    }

    private function setFilePath($file_path)
    {
        $this->file_path = $file_path;
    }
}