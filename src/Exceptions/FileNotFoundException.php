<?php

namespace GrotTheCheat\YamlConfig\Exceptions;

class FileNotFoundException extends \RuntimeException
{
    public function __construct(string $file_path)
    {
        parent::__construct("YamlConfig could not find the config file '" . $file_path . "'.");
    }
}